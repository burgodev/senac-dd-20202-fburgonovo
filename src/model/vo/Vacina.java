package model.vo;

import java.time.LocalDate;

public class Vacina {

	private String nome;
	private String nomePesquisador;
	private String pais;
	private int estagio; //1-inicial; 2-teste; 3-massa; 
	private LocalDate dataInicio;	
	
	public Vacina(String nome, String nomePesquisador, String pais, int estagio, LocalDate dataInicio) {
		super();
		this.nome = nome;
		this.nomePesquisador = nomePesquisador;
		this.pais = pais;
		this.estagio = estagio;
		this.dataInicio = dataInicio;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomePesquisador() {
		return nomePesquisador;
	}
	public void setNomePesquisador(String nomePesquisador) {
		this.nomePesquisador = nomePesquisador;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public int getEstagio() {
		return estagio;
	}
	public void setEstagio(int estagio) {
		this.estagio = estagio;
	}
	public LocalDate getDataInicio() {
		return dataInicio;
	}
	public void setDataInicio(LocalDate dataInicio) {
		this.dataInicio = dataInicio;
	}	

}
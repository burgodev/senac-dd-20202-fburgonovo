package model.vo;

import java.time.LocalDate;
import java.time.Period;

public class Publico {

	private String nome;
	private String cpf;
	private char sexo;	 
	private LocalDate dataNascimento;
	private boolean vacinada;
	private boolean voluntario;	
	private int avaliacao;
	
	public Publico(String nome, String cpf, char sexo, LocalDate dataNascimento, boolean vacinada, boolean voluntario,
			int avaliacao) {
		super();
		this.nome = nome;
		this.cpf = cpf;
		this.sexo = sexo;
		this.dataNascimento = dataNascimento;
		this.vacinada = vacinada;
		this.voluntario = voluntario;
		this.avaliacao = avaliacao;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public LocalDate getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(LocalDate dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public boolean isVacinada() {
		return vacinada;
	}
	public void setVacinada(boolean vacinada) {
		this.vacinada = vacinada;
	}
	public boolean isVoluntario() {
		return voluntario;
	}
	public void setVoluntario(boolean voluntario) {
		this.voluntario = voluntario;
	}
	public int getAvaliacao() {
		return avaliacao;
	}
	public void setAvaliacao(int avaliacao) {
		this.avaliacao = avaliacao;
	}
	
	
	
	
}
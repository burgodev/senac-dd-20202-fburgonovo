package model.vo;

public class Pesquisador {

	private String nome;
	private String instituicao;
	
	public Pesquisador(String nome, String instituicao) {
		super();
		this.nome = nome;
		this.instituicao = instituicao;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getInstituicao() {
		return instituicao;
	}
	public void setInstituicao(String instituicao) {
		this.instituicao = instituicao;
	}
	
	

}